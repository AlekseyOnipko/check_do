package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/digitalocean/godo"
	"github.com/go-co-op/gocron"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"log"
	"os"
	"strconv"
	"time"
)

func main() {
	f, logerr := os.OpenFile("info.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if logerr != nil {
		log.Fatal(logerr)
	}
	defer f.Close()

	log.SetOutput(f)

	s := gocron.NewScheduler(time.Local)
	_, err := s.Cron("0 6,18 * * *").Do(task)
	if err != nil {
		return
	}
	s.StartBlocking()
}

func init() {
	err := godotenv.Load()
	if err != nil {
		fmt.Println("No .env file found")
	}
}

func task() {
	token, exists := os.LookupEnv("DO_TOKEN")
	if !exists {
		log.Println("Не найден ключ DO")
		return
	}

	client := godo.NewFromToken(token)
	ctx := context.TODO()
	balance, _, err := client.Balance.Get(ctx)
	if err != nil {
		log.Println(err)
		return
	}
	sendMessage(balance.MonthToDateUsage)
}

func sendMessage(balance string) {
	token, exists := os.LookupEnv("TELEGRAM_TOKEN")
	if !exists {
		log.Println("Не найден ключ Телеграм")
	}

	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Println(err)
		return
	}
	chat, chaterr := getEnvAsInt64("CHAT_ID")
	if chaterr != nil {
		log.Println(chaterr)
		return
	}
	msg := fmt.Sprintf("В Digital Ocean израсходовано %s$ в этом месяце", balance)
	telegramMsg := tgbotapi.NewMessage(chat, msg)
	_, er := bot.Send(telegramMsg)
	if er != nil {
		log.Println(er)
	} else {
		log.Printf("В Digital Ocean израсходовано %s$ в этом месяце", balance)
	}

}
func getEnvAsInt64(name string) (
	int64,
	error,
) {
	valueStr, exists := os.LookupEnv(name)
	if !exists {
		return 0, errors.New("нет такого ключа")
	}
	var i64 int64
	if value, err := strconv.Atoi(valueStr); err == nil {

		i64 = int64(value)
	} else {
		return 0, errors.New("не удалость обработать данные")
	}
	return i64, nil
}
